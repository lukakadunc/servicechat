package com.kadunc.servicechat;

import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.WorkerThread;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kadunc.servicechat.utils.ErrorUtils;
import com.kadunc.servicechat.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityIRC extends MyActivity {
	private static final String TAG = "ActivityIRC";

	private RecyclerView mRecyclerView;
	private MessageAdapter mAdapter;

	private EditText messageView;

	private final OkHttpClient okHttpClient = new OkHttpClient();
	private String myUsername;
	private String authToken;

	private static final int REFRESH_TIME = 2000;
	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_irc);
		initMaterial();

		mHandler = new Handler();

		myUsername = PreferenceUtils.getDefault(this).getString(PreferenceUtils.KEY_USERNAME, null);
		authToken = PreferenceUtils.getAuthorizationToken(this);

		mRecyclerView = (RecyclerView) findViewById(R.id.irc_messages);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mAdapter = new MessageAdapter();
		mRecyclerView.setAdapter(mAdapter);

		messageView = (EditText) findViewById(R.id.irc_message);
		ImageButton sendButton = (ImageButton) findViewById(R.id.irc_send);
		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				sendMessage();
			}
		});

		callRefresh();
	}

	private final Runnable refreshRunnable = new Runnable() {
		@Override
		public void run() {
			callRefresh();

			//Osveži na 2 sekundi
			mHandler.postDelayed(refreshRunnable, REFRESH_TIME);
		}
	};

	@Override
	protected void onPause() {
		super.onPause();

		mHandler.removeCallbacks(refreshRunnable);
	}

	@Override
	protected void onResume() {
		super.onResume();

		mHandler.postDelayed(refreshRunnable, REFRESH_TIME);
	}

	private void sendMessage() {
		String message = messageView.getText().toString();
		messageView.setText(null);

		Request request = new Request.Builder()
				.url("http://chatis.azurewebsites.net/Service1.svc/Send/" + message)
				.header("REST-Authorization", authToken)
				.build();

		okHttpClient.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(ActivityIRC.this, "Napaka pri pošiljanju sporočila", Toast.LENGTH_SHORT).show();
					}
				});
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				refresh();
			}
		});
	}

	private void callRefresh() {
		new Thread() {
			@Override
			public void run() {
				Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

				refresh();
			}
		}.start();
	}

	@WorkerThread
	private synchronized void refresh() {
		LogUtils.log(TAG, "Refreshing ...");

		Request request = new Request.Builder()
				.url("http://chatis.azurewebsites.net/Service1.svc/Messages/" + (mAdapter.getLastId() + 1))
				.header("REST-Authorization", authToken)
				.build();

		try {
			Response response = okHttpClient.newCall(request).execute();

			String responseText = response.body().string();
			if(responseText.isEmpty()) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(ActivityIRC.this, "Napaka pri prijavi", Toast.LENGTH_SHORT).show();
					}
				});
				return;
			}

			JSONArray json = new JSONArray(responseText);

			final ArrayList<MessageElement> messages = new ArrayList<>();

			int len = json.length();
			for(int i = 0; i < len; i++) {
				JSONObject message = json.getJSONObject(i);

				int id = message.getInt("ID");
				String username = message.getString("username");
				String text = message.getString("besedilo");

				MessageElement element = new MessageElement(id, username, text);
				messages.add(element);
			}

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mAdapter.addData(messages);
				}
			});
		} catch(IOException e) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(ActivityIRC.this, "Napaka pri pridobivanju podatkov", Toast.LENGTH_SHORT).show();
				}
			});
		} catch(JSONException e) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(ActivityIRC.this, "Napaka v JSON", Toast.LENGTH_SHORT).show();
				}
			});

			ErrorUtils.logException(ActivityIRC.this, e);
		}
	}

	private static class MessageElement {
		final int id;
		final String username;
		final String message;

		private MessageElement(int id, String username, String message) {
			this.id = id;
			this.username = username;
			this.message = message;
		}
	}

	private class MessageViewHolder extends RecyclerView.ViewHolder {
		private final TextView usernameView;
		private final TextView messageView;

		public MessageViewHolder(ViewGroup layout) {
			super(layout);

			usernameView = (TextView) layout.findViewById(R.id.message_username);
			messageView  = (TextView) layout.findViewById(R.id.message_message);
		}

		void bind(MessageElement message) {
			usernameView.setText(message.username);
			messageView.setText(message.message);

			int gravity = message.username.equals(myUsername) ? Gravity.RIGHT : Gravity.LEFT;
			usernameView.setGravity(gravity);
			messageView.setGravity(gravity);
		}
	}

	private class MessageAdapter extends RecyclerView.Adapter<MessageViewHolder> {
		private ArrayList<MessageElement> data = new ArrayList<>();

		@Override
		public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			ViewGroup layout = (ViewGroup) LayoutInflater.from(ActivityIRC.this).inflate(R.layout.item_message, parent, false);
			return new MessageViewHolder(layout);
		}

		int getLastId() {
			if(data.isEmpty()) {
				return -1;
			}

			return data.get(data.size() - 1).id;
		}

		void addData(ArrayList<MessageElement> newData) {
			int oldCount = getItemCount();

			this.data.addAll(newData);

			notifyItemRangeInserted(oldCount, newData.size());

			if(oldCount == 0) {
				mRecyclerView.scrollToPosition(getItemCount() - 1);
			} else if(!newData.isEmpty()) {
				mRecyclerView.smoothScrollToPosition(getItemCount() - 1);
			}
		}

		@Override
		public void onBindViewHolder(MessageViewHolder holder, int position) {
			holder.bind(data.get(position));
		}

		@Override
		public int getItemCount() {
			return data.size();
		}
	}

	private void initMaterial() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
		if(toolbar != null) {
			setSupportActionBar(toolbar);
		}
	}
}
