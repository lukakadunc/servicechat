package com.kadunc.servicechat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityLogin extends MyActivity {
	private TextView usernameView;
	private TextView passwordView;
	private Button loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		usernameView = (TextView) findViewById(R.id.login_username);
		passwordView = (TextView) findViewById(R.id.login_password);

		loginButton = (Button) findViewById(R.id.login_login);
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//Da ne more 2x klikniti
				loginButton.setEnabled(false);

				login();
			}
		});

//		usernameView.setText("samo");
//		passwordView.setText("AAaa12!\"");
//		loginButton.performClick();

		//Ej, res se mi ne da vsakič tega not pisat
		SharedPreferences prefs = PreferenceUtils.getDefault(this);
		usernameView.setText(prefs.getString(PreferenceUtils.KEY_USERNAME, null));
		passwordView.setText(prefs.getString(PreferenceUtils.KEY_PASSWORD, null));
	}

	private void login() {
		final String username = usernameView.getText().toString();
		final String password = passwordView.getText().toString();

		String token = username + '|' + password;

		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
				.url("http://chatis.azurewebsites.net/Service1.svc/Login")
				.addHeader("REST-Authorization", token)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						loginButton.setEnabled(true);

						Toast.makeText(getApplicationContext(), "Napaka pri povezavi z internetom", Toast.LENGTH_SHORT).show();
					}
				});
			}

			@Override
			public void onResponse(final Call call, final Response response) throws IOException {
				final String text = response.body().string();

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							if("false".equals(text)) {
								loginButton.setEnabled(true);

								Toast.makeText(getApplicationContext(), "Napačno uporabniško ime ali geslo", Toast.LENGTH_SHORT).show();
								return;
							}

							if("true".equals(text)) {
								PreferenceUtils.login(ActivityLogin.this, username, password);

								startActivity(new Intent(getApplicationContext(), ActivityIRC.class));
								finish();
								return;
							}

							throw new IOException(text);
						} catch(IOException e) {
							Toast.makeText(getApplicationContext(), "Napaka pri povezavi z internetom", Toast.LENGTH_SHORT).show();
						} catch(Exception e) {
							loginButton.setEnabled(true);

							Toast.makeText(getApplicationContext(), "Neznana napaka", Toast.LENGTH_SHORT).show();
						}
					}
				});
			}
		});
	}
}
