package com.kadunc.servicechat;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.UiThread;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.kadunc.servicechat.utils.ErrorUtils;
import com.kadunc.servicechat.utils.LogUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public abstract class MyActivity extends AppCompatActivity {
	private static final String TAG = "MyActivity";

	public volatile boolean destroyed = false;

	private LinkedList<WeakReference<AlertDialog>> alertDialogs = new LinkedList<>();
	private LinkedList<WeakReference<android.app.AlertDialog>> alertDialogsLegacy = new LinkedList<>();

	public volatile SQLHelper sqlHelper; //volatile zaradi BroadcastReceiverja

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LogUtils.log("MyActivity", "Create " + getClass().getSimpleName());

		//najprej SQLHelper, ker recimo SettingsFragment na screen rotation rabi getCurrentUproabnik
		sqlHelper = SQLHelper.getInstance(getApplicationContext());

		super.onCreate(savedInstanceState);

		if(BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
			enableStrictMode();
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
	private void enableStrictMode() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectCustomSlowCalls()
				.detectNetwork()
				.penaltyFlashScreen()
				.penaltyLog()
				.penaltyDeath()
				.build());
		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectLeakedSqlLiteObjects()
				.detectLeakedClosableObjects()
//				.penaltyDeathOnCleartextNetwork()
//				.detectLeakedRegistrationObjects()
				.detectActivityLeaks()
				.detectFileUriExposure()
				.penaltyLog()
				.build());
	}

	@Override
	protected void onDestroy() {
		destroyed = true;

		//da ne leakamo windowa
		//http://stackoverflow.com/questions/11051172/progressdialog-and-alertdialog-cause-leaked-window

		while(alertDialogs.size() > 0) {
			WeakReference<AlertDialog> dialog = alertDialogs.pop();
			AlertDialog d = dialog.get();
			if(d != null) {
				dismissDialog(d);
			}
		}

		while(alertDialogsLegacy.size() > 0) {
			WeakReference<android.app.AlertDialog> dialog = alertDialogsLegacy.pop();
			android.app.AlertDialog d = dialog.get();
			if(d != null) {
				dismissDialog(d);
			}
		}

		super.onDestroy();

		//Ne smem zapret, ker je shared instance
		sqlHelper = null;
	}

	@Override
	protected void onStart() {
		super.onStart();

		LogUtils.log("MyActivity", "Start " + getClass().getSimpleName() + ": " + getToolbarTitle());
	}

	public static final int NET_OFFLINE    = 0;
	public static final int NET_CONNECTING = 1;
	public static final int NET_CONNECTED  = 2;
	public static int isOnline(Context context) {
		//getApplicationContext(), da preprečim memory leak
		//https://code.google.com/p/android/issues/detail?id=198852
		ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if(netInfo != null) {
			if(netInfo.isConnected()) {
				return NET_CONNECTED;
			} else if(netInfo.isConnectedOrConnecting()) {
				return NET_CONNECTING;
			}
		}
		return NET_OFFLINE;
	}
	public int isOnline() {
		return isOnline(this);
	}

	@Override
	public void setTitle(CharSequence title) {
		ActionBar bar = getSupportActionBar();
		if(bar != null) {
			bar.setTitle(title);
		}
	}

	protected CharSequence getToolbarTitle() {
		ActionBar bar = getSupportActionBar();
		if(bar != null) {
			return bar.getTitle();
		} else {
			return "";
		}
	}

	public static int dpiToPixel(int dpi) {
		float scale = Resources.getSystem().getDisplayMetrics().density;
		return (int) (dpi * scale +.5f);
	}
	public static float pixelToDpi(float px){
		float scale = Resources.getSystem().getDisplayMetrics().densityDpi;
		return DisplayMetrics.DENSITY_DEFAULT * px / scale;
	}

	public final boolean isLandscape() {
		return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}
	public final boolean isTablet() {
		return getResources().getBoolean(R.bool.is_tablet);
	}
	public final boolean isPhone() {
		return getResources().getBoolean(R.bool.is_phone);
	}
	public final boolean isWatch() {
		return getResources().getBoolean(R.bool.is_watch);
	}

	/**
	 * Upravljaj z AlertDialogom (zapri ga na onDestroy)
	 * @param dialog AlertDialog
	 * @return true, če je bil dialog prikazan
	 */
	@UiThread
	public boolean showDialog(AlertDialog dialog) {
		if(destroyed) {
			return false;
		}

		try {
			dialog.show();
			alertDialogs.push(new WeakReference<>(dialog));

			return true;
		} catch(IllegalArgumentException e) {
			if(!e.getMessage().contains("not attached to window manager")) {
				throw e;
			} else {
				//Drugače ignoriraj
				return false;
			}
		}
	}

	/**
	 * Upravljaj z AlertDialogom (zapri ga na onDestroy)
	 * @param dialog AlertDialog
	 * @return true, če je bil dialog prikazan
	 */
	@UiThread
	public boolean showDialog(android.app.AlertDialog dialog) {
		if(destroyed) {
			return false;
		}

		try {
			dialog.show();
			alertDialogsLegacy.push(new WeakReference<>(dialog));

			return true;
		} catch(IllegalArgumentException e) {
			if(!e.getMessage().contains("not attached to window manager")) {
				throw e;
			} else {
				//Drugače ignoriraj
				return false;
			}
		}
	}

	public static void dismissDialog(android.app.Dialog dialog) {
		try {
			dialog.dismiss();
		} catch(IllegalArgumentException e) {
			if(!e.getMessage().contains("not attached to window manager")) {
				ErrorUtils.logException(new RuntimeException("Dismiss fail", e));
			}
			//Drugače ignoriraj
		}
	}
	public static void dismissDialog(AlertDialog dialog) {
		try {
			dialog.dismiss();
		} catch(IllegalArgumentException e) {
			if(!e.getMessage().contains("not attached to window manager")) {
				ErrorUtils.logException(new RuntimeException("Dismiss fail", e));
			}
			//Drugače ignoriraj
		}
	}

	public void showMessage(final String title, final String message, final Runnable onClose, final String buttonText) {
		runOnUiThread(new Runnable() {
			@SuppressLint("InflateParams")
			@Override
			public void run() {
				final AlertDialog.Builder dialog = new AlertDialog.Builder(MyActivity.this, R.style.MyAlertDialogTheme);

				dialog.setTitle(title);

				if(message != null) {
					String text = message.replaceAll("\\n", "<br>");
					if(!isWatch()) {
						text = "<big>" + text + "</big>";
					}
					dialog.setMessage(Html.fromHtml(text));
				}

				dialog.setNeutralButton(buttonText, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				});

				if(onClose != null) {
					dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							onClose.run();
						}
					});
				}

				AlertDialog d = dialog.create();
				if(showDialog(d)) {
					((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
				}
			}
		});
	}
	public void showMessage(String title, String message, Runnable onClose) {
		showMessage(title, message, onClose, "V redu");
	}
	public void showMessage(String title, String message) {
		showMessage(title, message, null);
	}

	public void showYesNoMessage(final String title, final String message, final String textYes, final String textNo, final Runnable callbackYes, final Runnable callbackNo) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final AlertDialog.Builder dialog = new AlertDialog.Builder(MyActivity.this, R.style.MyAlertDialogTheme);
				dialog.setTitle(title);

				if(message != null) {
					String text = message.replaceAll("\\n", "<br>");
					if(!isWatch()) {
						text = "<big>" + text + "</big>";
					}
					dialog.setMessage(Html.fromHtml(text));
				}

				dialog.setPositiveButton(textYes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if(callbackYes != null) {
							callbackYes.run();
						}
					}
				});

				dialog.setNegativeButton(textNo, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if(callbackNo != null) {
							callbackNo.run();
						}
					}
				});

				AlertDialog d = dialog.create();
				if(showDialog(d)) {
					((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
				}
			}
		});
	}

	public void showYesNoNeutralMessage(final String title, final String message, final String textYes, final String textNo, final String textNeutral, final Runnable callbackYes, final Runnable callbackNo, final Runnable callbackNeutral) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final AlertDialog.Builder dialog = new AlertDialog.Builder(MyActivity.this, R.style.MyAlertDialogTheme);
				dialog.setTitle(title);

				if(message != null) {
					String text = message.replaceAll("\\n", "<br>");
					if(!isWatch()) {
						text = "<big>" + text + "</big>";
					}
					dialog.setMessage(Html.fromHtml(text));
				}

				dialog.setPositiveButton(textYes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if(callbackYes != null) {
							callbackYes.run();
						}
					}
				});

				dialog.setNegativeButton(textNo, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if(callbackNo != null) {
							callbackNo.run();
						}
					}
				});

				dialog.setNeutralButton(textNeutral, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if(callbackNeutral != null) {
							callbackNeutral.run();
						}
					}
				});

				AlertDialog d = dialog.create();
				if(showDialog(d)) {
					((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
				}
			}
		});
	}

	public void showInputDialog(String title, String message, String initialValue, String hint, String okText, final InputDialogCallback callback, int inputType, final String autocompleteSource) {
		final AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.MyAlertDialogTheme);
		dialog.setTitle(title);

		if(message != null) {
			String text = message.replaceAll("\\n", "<br>");
			if(!isWatch()) {
				text = "<big>" + text + "</big>";
			}
			dialog.setMessage(Html.fromHtml(text));
		}

		final SharedPreferences prefs;
		final Set<String> hints;

		final EditText editText;
		if(autocompleteSource == null) {
			editText = new AppCompatEditText(this);

			prefs = null;
			hints = null;
		} else {
			editText = new AppCompatAutoCompleteTextView(this);

			prefs = PreferenceUtils.getDefault(this);
			hints = prefs.getStringSet(autocompleteSource, new HashSet<String>());

			ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, new ArrayList<>(hints));
			((AppCompatAutoCompleteTextView)editText).setAdapter(adapter);
			((AppCompatAutoCompleteTextView)editText).setThreshold(0);
			((AppCompatAutoCompleteTextView)editText).showDropDown();
		}

		editText.setInputType(inputType);
		editText.setSingleLine();
		editText.setHint(hint);
		editText.setText(initialValue);

		int dpi12 = dpiToPixel(12);
		int dpi20 = dpiToPixel(20);
		dialog.setView(editText, dpi20, dpi12, dpi20, 0);

		dialog.setPositiveButton(okText, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String value = editText.getText().toString();

				if(hints != null) {
					hints.add(value);
					prefs.edit().putStringSet(autocompleteSource, hints).apply();
				}

				callback.returnValue(value);
			}
		});

		dialog.setNegativeButton("Prekliči", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				callback.returnValue(null);
			}
		});

		if(destroyed) {
			return;
		}

		AlertDialog d = dialog.create();
		d.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				//Odpri tipkovnico
				editText.requestFocus();
				InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.toggleSoftInputFromWindow(editText.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
			}
		});

		showDialog(d);
	}
	public interface InputDialogCallback {
		/**
		 * @param value Vrednost ali null, če je uporabnik zaprl okno
		 */
		void returnValue(String value);
	}
}
