package com.kadunc.servicechat;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class PreferenceUtils {
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";

	public static SharedPreferences getDefault(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static void login(Context context, String username, String password) {
		getDefault(context).edit()
				.putString(KEY_USERNAME, username)
				.putString(KEY_PASSWORD, password)
				.apply();
	}

	public static String getAuthorizationToken(Context context) {
		SharedPreferences prefs = getDefault(context);

		String username = prefs.getString(KEY_USERNAME, null);
		String password = prefs.getString(KEY_PASSWORD, null);

		if(username == null || password == null) {
			throw new IllegalStateException("Nisi prijavljen");
		}

		return username + '|' + password;
	}
}
