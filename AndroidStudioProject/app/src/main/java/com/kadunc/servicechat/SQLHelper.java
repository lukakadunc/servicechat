package com.kadunc.servicechat;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import java.io.File;

public final class SQLHelper extends SQLiteOpenHelper {
	private static final String TAG = "SQLHelper";

	static final int DATABASE_VERSION = 1;

	private final Context context;

	private SQLHelper(Context context, String dbName) {
		super(context, dbName, null, DATABASE_VERSION);
		this.context = context;
	}

	public static SQLHelper getInstance(Context context) {
		return new SQLHelper(context, getDbPath(context).getAbsolutePath());
	}

	public static File getDbPath(Context context) {
		File mapa = context.getFilesDir();
		return new File(mapa, "ServiceChat.db");
	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		if(!database.isDatabaseIntegrityOk()) {
			throw new RuntimeException("Integriteta baze ni OK!");
		}
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if(!db.isReadOnly()) {
			//Da dela on delete cascade
			//Android 2.2+
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.beginTransaction();
		try {

			db.setTransactionSuccessful();
		} catch(SQLException e) {
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				throw new SQLException("Nisem uspel posodobiti baze iz " + oldVersion + " na " + newVersion, e);
			} else {
				//Ne podpira konstruktorja SQLException(message, cause)
				throw new SQLException("Nisem uspel posodobiti baze iz " + oldVersion + " na " + newVersion);
			}
		} finally {
			db.endTransaction();
		}

		if(oldVersion != newVersion) {
			throw new SQLException("Nisem uspel posodobiti baze iz " + oldVersion + " na " + newVersion);
		}
	}
}
