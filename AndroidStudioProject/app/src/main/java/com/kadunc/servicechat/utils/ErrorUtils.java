package com.kadunc.servicechat.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.kadunc.servicechat.BuildConfig;

import java.net.SocketTimeoutException;

public abstract class ErrorUtils {
	private static final String TAG = "Log";

	public static String getStackTrace(Throwable e) {
		if(e == null) {
			return "Ni podrobnosti";
		}

		if(e instanceof SocketTimeoutException) {
			return "Časovna omejitev za povezavo je potekla.";
		}

		String message = e.getClass().getSimpleName();

		if(e.getMessage() != null) {
			message += ": ";
			message += e.getMessage();
		}

		if(e.getCause() != null) {
			message += "\n\nZaradi: ";
			message += getStackTrace(e.getCause());
		}

		return message;
	}

	public static void logException(final Context context, final Throwable e) {
		if(BuildConfig.DEBUG) {
			new Handler(context.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, "EXCEPTION\n" + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			});
			Log.e(TAG, "Silent exception", e);
		}
	}

	public static void logException(final Context context, final Throwable e, String tag, String data) {
		if(BuildConfig.DEBUG) {
			new Handler(context.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, "EXCEPTION\n" + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			});
			Log.e(TAG, "Silent exception", e);
			Log.e(TAG, tag + ": " + data);
		}
	}

	public static void logException(final Throwable e) {
		if(BuildConfig.DEBUG) {
			Log.e(TAG, "Silent exception", e);
		}
	}

	public static void logException(final Throwable e, String tag, String data) {
		if(BuildConfig.DEBUG) {
			Log.e(TAG, "Silent exception", e);
			Log.e(TAG, tag + ": " + data);
		}
	}
}
