package com.kadunc.servicechat.utils;

import android.util.Log;

import com.kadunc.servicechat.BuildConfig;

public abstract class LogUtils {
	public static void log(String tag, String message) {
		if(BuildConfig.DEBUG) {
			Log.d(tag, message);
		}
	}
}
