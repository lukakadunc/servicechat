package com.kadunc.servicechat.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {
	/**
	 * http://stackoverflow.com/a/6565597/971972
	 * @param input Bytes
	 * @return MD5 hash
	 */
	public static String md5(byte[] input) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch(NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		byte[] bytes = md.digest(input);

		StringBuilder sb = new StringBuilder(32);
		for(byte aByte : bytes) {
			sb.append(Integer.toHexString((aByte & 0xFF) | 0x100).substring(1, 3));
		}
		return sb.toString();
	}
}
