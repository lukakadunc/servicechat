# ChatDB
Link:
http://chatis.azurewebsites.net/ChatDB/Login.aspx

## Avtorja
Samo Remec, 63150241  
Luka Kadunc, 63150136

## Naloge avtorjev

### Luka Kadunc
- Azure(vse kar se tiče tega)
- REST
- Povezava spletne strani z RESTI-i
- Front end


### Samo Remec
- Android
- Administratorski vmesnik
- README
- Code review

## Registrirani uporabniki
- uporabniško ime: kadunc123  
  geslo: lukaJEkadunc1234!
- uporabniško ime: kadunc  
  geslo: kaduncAA11!
- uporabniško ime: samo  
  geslo: AAaa12!"

## Zaslonski posnetki
![Image1.jpg](https://bitbucket.org/repo/r4baL9/images/390663508-Image1.jpg)
![Screenshot (52).png](https://bitbucket.org/repo/r4baL9/images/136790755-Screenshot%20(52).png)
![Screenshot (53).png](https://bitbucket.org/repo/r4baL9/images/2605389837-Screenshot%20(53).png)
![Screenshot_20170115-234306.png](https://bitbucket.org/repo/r4baL9/images/3231182885-Screenshot_20170115-234306.png)
![Screenshot_20170115-234320.png](https://bitbucket.org/repo/r4baL9/images/1698226996-Screenshot_20170115-234320.png)

## Problemi, na katere smo naleteli
Nič se ni spremenilo

![vs_shit.png](https://bitbucket.org/repo/r4baL9/images/2125473419-vs_shit.png)

## Struktura baze

Pa ista je, samo tabela Uporabnik ima novo polje admin tipa bit.