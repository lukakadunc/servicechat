﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chat.aspx.cs" Inherits="NoDB.Chat2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NoDB - chat</title>
    <style>
        * {
            color: black;
            font-size: 17px;
            font-weight: 400;
            font-family: Arial, sans-serif;
        }

        .button {
            background: #bebebe;
            border: 1px solid black;
            border-radius: 5px;
            height: 40px;
            width: 100%;
        }

        .box {
            border: 1px solid black;
            vertical-align: top;
            padding: 10px;
        }

        table {
            border-spacing: 10px;
            box-shadow: rgba(0,0,0, 0.4) 0 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table style="width: 900px">
            <tr>
                <td>Prijavljeni ste kot <asp:Label ID="CurrentUser" runat="server" /></td>
                <td>
                    <asp:Button ID="Logout" runat="server" Text="Odjava" OnClick="ButtonLogout_Click" CssClass="button" />
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="height: 400px; width: 80%" class="box">
                    <asp:Label ID="Messages" runat="server" />
                </td>
                <td style="height: 100%; width: 20%;" class="box">
                    <asp:Label ID="Users" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Refresh" runat="server" Text="Osveži" OnClick="ButtonRefresh_Click" CssClass="button" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="Message" runat="server" style="width: 100%; height: 100%; border: 1px solid black;" />
                </td>
                <td>
                    <asp:Button ID="Send" runat="server" Text="Pošlji" OnClick="ButtonSend_Click" CssClass="button" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
