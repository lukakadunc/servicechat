﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace NoDB {
    public partial class Chat2 : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            //Preveri, če je uporabnik prijavljen
            if (Session["logged_in"] == null) {
                Response.Redirect("Login.aspx");
                Response.End();
            }

            //Prijavljen kot ...
            CurrentUser.Text = ((NoDB.User) Session["user"]).naziv;
        }

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            //Sprintaj vsa sporočila
            string allMessages = "";
            foreach (Message m in NoDB.Message.getMessages()) {
                allMessages += m.username + ": " + m.message;
                allMessages += "<br />";
            }
            Messages.Text = allMessages;

            string allUsers = "";
            foreach (User u in NoDB.User.loggedIn) {
                allUsers += u.naziv + "<br />";
            }
            Users.Text = allUsers;
        }

        protected void ButtonSend_Click(object sender, EventArgs e) {
            Message newMessage = new Message(((NoDB.User)Session["user"]).naziv, Message.Text);
            NoDB.Message.insert(newMessage);
            Message.Text = "";
        }

        protected void ButtonRefresh_Click(object sender, EventArgs e) {
            //Realno ne naredi nič
        }

        protected void ButtonLogout_Click(object sender, EventArgs e) {
            NoDB.User.loggedIn.Remove(Session["user"]);

            Session["logged_in"] = null;
            Session["user"] = null;

            Response.Redirect("Login.aspx");
            Response.End();
        }
    }

    public class Message {
        public readonly string username;
        public readonly string message;

        public Message(DataRow row) {
            username = row["username"].ToString();
            message  = row["besedilo"].ToString().Trim();
        }

        public Message(string username, string message) {
            this.username = username;
            this.message = message;
        }

        static string path = "|DataDirectory|\\chatdb.mdf";
        static string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";

        public static void insert(Message m) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("insert into Pogovor (username, besedilo) values (@username, @besedilo)", connection)) {
                    command.Parameters.Add(new SqlParameter("username", m.username));
                    command.Parameters.Add(new SqlParameter("besedilo", m.message));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public static List<Message> getMessages() {
            DataTable data = new DataTable("Pogovor");
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("Select * from Pogovor", connection)) {
                    using (SqlDataAdapter da = new SqlDataAdapter(command)) {
                        da.Fill(data);
                    }
                }
                connection.Close();
            }

            //razbijemo data vrstice, v vsiUporabniki
            List<Message> all = new List<Message>();
            foreach (DataRow row in data.Rows) {
                Message message = new Message(row);
                all.Add(message);
            }

            return all;
        }
    }
}