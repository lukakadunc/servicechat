﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Text;
using System.Security.Cryptography;

namespace NoDB
{
    public class LogedUsers
    {
        private static List<string> users = new List<string>();

        public static void add(string u)
        {
            users.Add(u);
        }

        public static void delete(string username)
        {
            users.Remove(username);
        }

        public static List<string> getLogedUsers()
        {
            return users;
        }
    }

    public class User1
    {
        public string username { get; set; }
        public string Ime { get; set; }
        public string Priime { get; set; }
        public string Geslo { get; set; }
        public Boolean Admin { get; set; }
    }

    public class Messages
    {
        public int ID { get; set; }
        public string username { get; set; }
        public string besedilo { get; set; }

    }

    public class ConnectingToDatabase
    {
        static string source = "Data Source = chatisserver.database.windows.net; Initial Catalog=isdatabase; Persist Security Info=True;User ID = kadunc; Password=12345Luka";


        public static List<User1> getUsers()
        {
            List<User1> users = new List<User1>();

            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand select = new SqlCommand("SELECT * FROM Uporabnik", connection))
                {
                    using (SqlDataReader reader = select.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            User1 temp = new User1();
                            temp.username = reader["username"].ToString();
                            temp.Ime      = reader["ime"].ToString();
                            temp.Priime   = reader["priimek"].ToString();

                            var i = reader.GetOrdinal("admin"); // Get the field position
                            temp.Admin = reader.GetBoolean(i);

                            users.Add(temp);
                        }
                    }
                }
                connection.Close();
            }
            return users;
            //return termp;
        }

        public static void deleteUser(string username) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();

                using (var cmd = connection.CreateCommand()) {
                    cmd.CommandText = "DELETE FROM Pogovor WHERE username = @username";
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.ExecuteNonQuery();
                }

                using (var cmd = connection.CreateCommand()) {
                    cmd.CommandText = "DELETE FROM Uporabnik WHERE username = @username";
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public static void makeAdmin(string username) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();

                using (var cmd = connection.CreateCommand()) {
                    cmd.CommandText = "UPDATE Uporabnik SET admin=1 WHERE username = @username";
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public static void makeNormal(string username) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();

                using (var cmd = connection.CreateCommand()) {
                    cmd.CommandText = "UPDATE Uporabnik SET admin=0 WHERE username = @username";
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        //za loginat vrne true če je v bazi
        public static bool Login(string username, string password)
        {
           
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand select = new SqlCommand(@"SELECT username 
                                                           FROM Uporabnik 
                                                           WHERE username = @username and geslo=@paswd", connection))
                {
                    select.Parameters.AddWithValue("@username", username);
                    select.Parameters.AddWithValue("@paswd", CalculateMD5Hash(password));
                    using (SqlDataReader reader = select.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            connection.Close();
                            return true;   
                       
                        }
                    }
                }
                connection.Close();
            }
            return false;
        }

        //vrne, če je človek administrator
        public static bool isAdmin(string username) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand select = new SqlCommand(@"SELECT admin
                                                           FROM Uporabnik 
                                                           WHERE username = @username", connection)) {
                    select.Parameters.AddWithValue("@username", username);
                    using (SqlDataReader reader = select.ExecuteReader()) {
                        while(reader.Read()) {
                            Boolean jeAdmin = Convert.ToBoolean(reader["admin"]);
                            connection.Close();
                            return jeAdmin;
                        }
                    }
                }
                connection.Close();
            }
            return false;
        }

        //vrne vsa sporočila
        public static List<Messages> getMessages(int id)
        {
            List<Messages> sporocila = new List<Messages>();
            

            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand select = new SqlCommand(@"SELECT id, besedilo, username 
                                                            FROM Pogovor
                                                            WHERE Id >= @id", connection))
                {
                    select.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader reader = select.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                           
                                Messages temp = new Messages();
                                temp.ID= Convert.ToInt32(reader["id"]);
                                temp.username = reader["username"].ToString();
                                temp.besedilo = reader["besedilo"].ToString();

                                sporocila.Add(temp);
                            
                        }
                    }
                }
                connection.Close();
            }
            return sporocila;

        }

        //Send messages
        public static void sendMessages(string sporocilo, string username)
        {
           
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                string saveStaff = "INSERT into Pogovor (username, besedilo) VALUES ('"+username+"', '"+sporocilo+"')";

                SqlCommand qSaveStaff = new SqlCommand(saveStaff, connection);
                qSaveStaff.ExecuteNonQuery();
                connection.Close();
            }
            
        }

        public static void insertUser(User1 u)
        {
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("insert into Uporabnik (ime, geslo, username) values (@ime, @geslo, @username)", connection))
                {
                    command.Parameters.Add(new SqlParameter("ime", u.Ime));
                    command.Parameters.Add(new SqlParameter("geslo", u.Geslo));
                    command.Parameters.Add(new SqlParameter("username", u.username));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        //MD 5 class
        public static string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();
        }
    }
}