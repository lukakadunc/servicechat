﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NoDB
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebGet(UriTemplate = "Login1", ResponseFormat = WebMessageFormat.Json)]
        string DoWork();

        [OperationContract]
        [WebGet(UriTemplate = "Login", ResponseFormat = WebMessageFormat.Json)]
        bool Login();

        [OperationContract]
        [WebGet(UriTemplate = "IsAdmin", ResponseFormat = WebMessageFormat.Json)]
        bool isAdmin();

        [OperationContract]
        [WebGet(UriTemplate = "ListUsers", ResponseFormat = WebMessageFormat.Json)]
        List<User1> listUsers();

        /*
        [OperationContract]
        [WebGet(UriTemplate = "Messages", ResponseFormat = WebMessageFormat.Json)]
        List<Messages> getMessages();*/


        [OperationContract]
        [WebGet(UriTemplate = "Messages/{id}", ResponseFormat = WebMessageFormat.Json)]
        List<Messages> getMessages(string id);

        [OperationContract]
        [WebGet(UriTemplate = "Send/{besedilo}", ResponseFormat = WebMessageFormat.Json)]
        void sendMessages(string besedilo);

    }
}
