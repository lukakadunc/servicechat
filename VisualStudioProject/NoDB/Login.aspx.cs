﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace NoDB {
    public partial class Login : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            //Si že prijavljen, kaj delaš tukaj?
            if (Session["logged_in"] != null) {
                Response.Redirect("Chat.aspx");
                Response.End();
            }
        }

        protected void LoginBtn_Click(object sender, EventArgs e) {
            string user  = Username.Text;
            string pass1 = Password.Text;
            string pass  = CalculateMD5Hash(pass1);

            foreach (User u in NoDB.User.getUsers()) {
                Boolean geslo = u.password.Equals(pass);
                Boolean ime   = u.username.Equals(user);

                if (u.username.Equals(user) && u.password.Equals(pass)) {
                    Session["logged_in"] = true;
                    Session["user"]      = u;

                    NoDB.User.loggedIn.Add(u);

                    Response.Redirect("Chat.aspx");

                    return;
                }
            }
            
            Username.Text = "";
            Password.Text = "";
        }

        protected void RegistrationBtn_Click(object sender, EventArgs e) {
            String name     = rName.Text;
            String username = rUsername.Text;
            String passwd   = rPassword.Text;
            String passwd1  = rPassword1.Text;

            if (passwd.Length < 8) {
                info.Text = "Geslo je prekratko! Vsebovati mora vsaj 8 znakov";
                info.ForeColor = System.Drawing.Color.Red;
                return;
            }

            //Če gelso ustreza zahtevam -- ni se mi dal s regexi zajebavat
            int special = 0;
            int upper   = 0;
            int number  = 0;

            foreach(char c in passwd) {
                if (c == '?' || c == '.' || c == '*' || c == '!' || c == ':')
                    special++;
                if (char.IsUpper(c))
                    upper++;
                if (Char.IsDigit(c))
                    number++;
            }

            if(special >= 1 && upper >= 2 && number >= 2) {
                //REGISTRACIJA JE USPEŠNA
                if(passwd1 == passwd) {
                    info.ForeColor = System.Drawing.Color.Green;
                    info.Text = "Registracija uspešna";
                    //tuki te pošlje naprej

                    User user = new User(name, username, CalculateMD5Hash(passwd));
                    NoDB.User.insert(user); //pokčičem metodo, ki zapiše v bazo
                } else {
                    //REGISTRACIJA NI USPEŠNA
                    info.ForeColor = System.Drawing.Color.Red;
                    info.Text = "Gesli se morata ujemati!";
                }
            } else {
                info.Text = "Registracija ni uspešna! Mora vsebovati 2 veliki črki, 2 števki in 1 poseben znak (? : . !)";
                info.ForeColor = System.Drawing.Color.Red;
            }
        }

        //MD 5 class
        public static string CalculateMD5Hash(string input) {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < hash.Length; i++) {
                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();
        }
    }

    public class User {
        public static ArrayList loggedIn = new ArrayList();

        public readonly string naziv;
        public readonly string username;
        public readonly string password;

        public User(DataRow row) {
            naziv    = row["ime"].ToString();
            username = row["username"].ToString();
            password = row["geslo"].ToString().Trim();
        }

        public User(string naziv, string username, string password) {
            this.naziv    = naziv;
            this.username = username;
            this.password = password;
        }

        static string path = "|DataDirectory|\\chatdb.mdf";
        static string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";

        //pisanje v bazo
        public static void insert(User u) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("insert into Uporabnik (ime, geslo, username) values (@ime, @geslo, @username)", connection)) {
                    command.Parameters.Add(new SqlParameter("ime", u.naziv));
                    command.Parameters.Add(new SqlParameter("geslo", u.password));
                    command.Parameters.Add(new SqlParameter("username", u.username));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        //branje iz baze
        //metoda za pridobitev vseh uporabnikov iz baze
        public static List<User> getUsers() {
            DataTable data = new DataTable("User");
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("Select * from Uporabnik", connection)) {
                    using (SqlDataAdapter da = new SqlDataAdapter(command)) {
                        da.Fill(data);
                    }
                }
                connection.Close();
            }

            //razbijemo data vrstice, v vsiUporabniki
            List<User> vsiUporabniki = new List<User>();
            foreach (DataRow row in data.Rows) {
                User user = new User(row);
                vsiUporabniki.Add(user);
            }

            return vsiUporabniki;
        }
    }
}