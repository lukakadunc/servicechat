﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NoDB
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string DoWork()
        {
            return "";//ConnectingToDatabase.getUsers();
        }

        public List<User1> DoWork1()
        {
            return ConnectingToDatabase.getUsers();
        }

        //Preverjanje če je uporabnik avtoticiran
        public bool Login()
        {
            //Pridobivanje usernama in pwdja iz headerja
            WebOperationContext ctx = WebOperationContext.Current;
            string authHeader = ctx.IncomingRequest.Headers["REST-Authorization"];
            if (authHeader == null)
                return false;

            string[] loginData = authHeader.Split('|');
            if (loginData.Length == 2 && ConnectingToDatabase.Login(loginData[0], loginData[1]))
                return true;
                // return ConnectingToDatabase.Login("kadunc", "5eb3f40c09a3b2311a66949c9f41b872");
            return false;
        }

        public bool isAdmin() {
            //Pridobivanje usernama in pwdja iz headerja
            WebOperationContext ctx = WebOperationContext.Current;
            string authHeader = ctx.IncomingRequest.Headers["REST-Authorization"];
            if (authHeader == null)
                return false;

            string[] loginData = authHeader.Split('|');
            if (loginData.Length == 2 && ConnectingToDatabase.Login(loginData[0], loginData[1]))
                if (ConnectingToDatabase.isAdmin(loginData[0]))
                    return true;
            
            return false;
        }

        public List<Messages> getMessages() {
            if(Login()) {
                return ConnectingToDatabase.getMessages(0);
            }
                
            return null;
        }
        
        public List<Messages> getMessages(string id) {
            int i = Convert.ToInt32(id);
            if(Login()) {
                return ConnectingToDatabase.getMessages(i);
            }

            return null;
        }

        public List<User1> listUsers() {
            if(Login() && isAdmin()) {
                return ConnectingToDatabase.getUsers();
            }

            return null;
        }

        public Boolean deleteUser(string username) {
            if(Login() && isAdmin()) {
                ConnectingToDatabase.deleteUser(username);
            }

            return false;
        }


       public void sendMessages(string besedilo)
        {
            if (Login())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                string authHeader = ctx.IncomingRequest.Headers["REST-Authorization"];
             
                string[] loginData = authHeader.Split('|');
                ConnectingToDatabase.sendMessages(besedilo, loginData[0]);
            }
        }
    }
}
