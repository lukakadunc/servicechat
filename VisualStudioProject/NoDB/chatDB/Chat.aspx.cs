﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace NoDB {
    public partial class Chat : System.Web.UI.Page {
        private readonly string URLGetMessages = "http://chatis.azurewebsites.net/Service1.svc/messages/";
        private readonly string URLSendMessages = "http://chatis.azurewebsites.net/Service1.svc/send/";
        private int nextMessage = 0;
        private string aut;
        List<string> loged;
        

        protected void Page_Load(object sender, EventArgs e) {
            //Preveri, če je uporabnik prijavljen
            if (Session["logged_in"] == null) {
                Response.Redirect("Login.aspx");
                Response.End();
            }

            //Prijavljen kot ...
            CurrentUser.Text = Session["user"].ToString();
            aut = Session["user"].ToString() + "|" + Session["pass"].ToString();
            getMessages();
        }

        //uporabniki ki so prijavljeni
        protected override void OnPreRender(EventArgs e) {
           
            base.OnPreRender(e);

            //Sprintaj vsa sporočila
            string allMessages = "";

            if (Session["nextMessage"] != null)
                nextMessage = Convert.ToInt32(Session["nextMessage"]);

            if (Session["allMessages"] != null)
                allMessages = Session["allMessages"].ToString();

            //Boolean isAdmin = //chatDB.admin.Users.isAdmin();
                //NoDB.Users.isAdmin();

            foreach (Messages m in getMessages()) {
                allMessages += m.username + ": " + m.besedilo;
                allMessages += "<br />";
                if (nextMessage <= m.ID)
                {
                    nextMessage = m.ID + 1;
                }
            }
            Session["allMessages"] = allMessages;
            Session["nextMessage"] = nextMessage;
            Messages.Text = allMessages;

            string allUsers = "";
            loged = LogedUsers.getLogedUsers();
            foreach(string u in loged)
            {
                allUsers += u + "<br/>";
            }
            Users.Text = allUsers;
        }

        protected List<Messages> getMessages()
        {
            //return ConnectingToDatabase.getMessages(nextMessage);
            
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URLGetMessages + nextMessage);
            client.DefaultRequestHeaders.Add("REST-Authorization", aut);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                string temp = response.Content.ReadAsStringAsync().Result;
                return (List<Messages>)Newtonsoft.Json.JsonConvert.DeserializeObject(temp, typeof(List<Messages>));
            }
            return null;
        }

        protected void ButtonSend_Click(object sender, EventArgs e) {

            Messages newMessage = new Messages();// ((NoDB.User)Session["user"]).naziv, Message.Text);
            newMessage.username = Session["user"].ToString();

            newMessage.besedilo = Message.Text;

            HttpClient client = new HttpClient();
            string URL = URLSendMessages + newMessage.besedilo;
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Add("REST-Authorization", aut);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
            }
            Message.Text = "";
        }

        protected void ButtonRefresh_Click(object sender, EventArgs e) {
            //Realno ne naredi nič
        }

        protected void ButtonLogout_Click(object sender, EventArgs e) {

            //NoDB.User.loggedIn.Remove(Session["user"]);

            LogedUsers.delete(Session["user"].ToString());

            Session["logged_in"] = null;
            Session["user"] = null;

            Response.Redirect("Login.aspx");
            Response.End();
        }
    }
    /*
    public class Message {
        public readonly string username;
        public readonly string message;

        public Message(DataRow row) {
            username = row["username"].ToString();
            message  = row["besedilo"].ToString().Trim();
        }

        public Message(string username, string message) {
            this.username = username;
            this.message = message;
        }

        static string path = "|DataDirectory|\\chatdb.mdf";
        static string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";

        public static void insert(Message m) {
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("insert into Pogovor (username, besedilo) values (@username, @besedilo)", connection)) {
                    command.Parameters.Add(new SqlParameter("username", m.username));
                    command.Parameters.Add(new SqlParameter("besedilo", m.message));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
      
        public static List<Message> getMessages() {
            DataTable data = new DataTable("Pogovor");
            using (SqlConnection connection = new SqlConnection(source)) {
                connection.Open();
                using (SqlCommand command = new SqlCommand("Select * from Pogovor", connection)) {
                    using (SqlDataAdapter da = new SqlDataAdapter(command)) {
                        da.Fill(data);
                    }
                }
                connection.Close();
            }

            //razbijemo data vrstice, v vsiUporabniki
            List<Message> all = new List<Message>();
            foreach (DataRow row in data.Rows) {
                Message message = new Message(row);
                all.Add(message);
            }

            return all;
        }
    }*/
}