﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NoDB.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NoDB - login</title>
        <style>
        * {
            font-family: Arial, sans-serif;
        }

        h1 {
            margin-bottom: 0;
        }

        i {
            color: gray;
        }

        table {
            color: black;
            font-size: 17px;
            font-weight: 400;
        }

        .button {
            background: #dedede;
            border: 1px solid black;
            border-radius: 5px;
            height: 30px;
            padding: 0 20px;
        }
    </style>
</head>
<body style="text-align: center">

    <h1>ChatDB</h1>

    <br />

    <form id="form1" runat="server">
        
        <div style="display: inline-flex;">

            <div style="margin-right: 150px;">
                <table>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <h1>Registracija</h1>

                            <i>Novi uporabniki</i>
                            <br /><br />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Ime:</td>
                        <td><asp:TextBox ID="rName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Uporabniško Ime:</td>
                        <td><asp:TextBox ID="rUsername" runat="server"></asp:TextBox></td>
                    </tr>
                        <tr>
                        <td style="text-align: right">Geslo:</td>
                        <td><asp:TextBox ID="rPassword" TextMode="password" runat="server" Height="20px"></asp:TextBox></td>
                    </tr>
                        <tr>
                        <td style="text-align: right">Geslo:</td>
                        <td><asp:TextBox ID="rPassword1" TextMode="password" runat="server"></asp:TextBox></td>
                    </tr>

                    <td colspan="2" style="text-align: right">
                        <asp:Button ID="RegistrationBtn" runat="server" Text="Registracija" OnClick="RegistrationBtn_Click" CssClass="button" />
                    </td>
                </table>

                <p>
                    <asp:Label ID="info" runat="server"></asp:Label>
                </p>
            </div>

            <div>
                    <table>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <h1>Prijava</h1>

                                <i>Obstoječi uporabniki</i>
                                <br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Uporabniško ime:</td>
                            <td><asp:TextBox ID="Username" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Geslo:</td>
                            <td><asp:TextBox ID="Password" TextMode="password" runat="server"></asp:TextBox></td>
                        </tr>

                        <td colspan="2" style="text-align: right">
                            <asp:Button ID="LoginAdmin" runat="server" Text="Admin plošča" OnClick="LoginAdmin_Click" CssClass="button" />
                            <asp:Button ID="LoginBtn" runat="server" Text="Prijava" OnClick="LoginBtn_Click" CssClass="button" />
                        </td>
                    </table>

            </div>

        </div>

    </form>

</body>
</html>
