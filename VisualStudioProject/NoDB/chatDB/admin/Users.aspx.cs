﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;

namespace NoDB.chatDB.admin {
    public partial class Users : System.Web.UI.Page {
        private static readonly string URLIsAdmin = "http://chatis.azurewebsites.net/Service1.svc/isAdmin";
        private static string aut;

        protected void Page_Load(object sender, EventArgs e) {
            //Preveri, če je uporabnik prijavljen
            if(Session["logged_in"] == null) {
                Response.Redirect("../Login.aspx");
                Response.End();
                return;
            }

            aut = Session["user"].ToString() + "|" + Session["pass"].ToString();

            if(!isAdmin()) {
                Response.Redirect("../Login.aspx");
                Response.End();
                return;
            }

            //Če sem kliknil, da zbriše uporabnika, ga dejansko zbriši
            string toDelete = Request.QueryString["delete"];
            if(toDelete != null) {
                ConnectingToDatabase.deleteUser(toDelete);
            }

            string make_admin = Request.QueryString["make_admin"];
            if(make_admin != null) {
                ConnectingToDatabase.makeAdmin(make_admin);
            }

            string make_normal = Request.QueryString["make_normal"];
            if (make_normal != null) {
                ConnectingToDatabase.makeNormal(make_normal);
            }
        }

        protected override void OnPreRender(EventArgs e) {
            string bodyBuilder = ""; //kadunc = bodybuilder

            foreach(User1 u in getUsers()) {
                bodyBuilder += u.username + ": " + u.Ime + " <a href='Users.aspx?delete=" + u.username + "'>[Izbriši]</a> ";

                if(u.Admin) {
                    bodyBuilder += "<a href='Users.aspx?make_normal=" + u.username + "'>[Naredi uporabnika]</a>";
                } else {
                    bodyBuilder += "<a href='Users.aspx?make_admin=" + u.username + "'>[Naredi administratorja]</a>";
                }

                bodyBuilder += "<br />";
            }

            UsersLabel.Text = bodyBuilder;
        }

        private List<User1> getUsers() {
            return ConnectingToDatabase.getUsers();
        }

        public static Boolean isAdmin() {
            HttpClient client = new HttpClient();
            string URL = URLIsAdmin;
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Add("REST-Authorization", aut);

            // List data response.
            HttpResponseMessage response = client.GetAsync("").Result;  //Blocking call!
            if (response.IsSuccessStatusCode) {
                String body = response.Content.ReadAsStringAsync().Result;
                return body == "true";
            }

            return false;
        }
    }
}